'use strict';

describe('The main page', function() {

  it('should hava a title', function() {
    browser.get('http://127.0.0.1:8080/#!/main');

    expect(element(by.id('hola')).getText()).toEqual('Hola');
  });  
});
