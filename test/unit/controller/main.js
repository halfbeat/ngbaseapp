'use strict';

var chai = require('chai')
  , expect = chai.expect;

var PageFactoryModule = require('../../../app/js/factories/page.js');
var MainCtrlModule = require('../../../app/js/controllers/main.js');

describe('The MainCtrl', function() {
  var $scope;

  beforeEach(function() {
    $scope = {      
    };

    MainCtrlModule($scope, PageFactoryModule());
  });

  it('should hava an empty list', function() {
    var list = $scope.list;
    expect(list).to.be.instanceof(Array);
    expect(list.length).to.equal(0);
  });
});
