'use strict';

//var css = require('../css/app.css');

require('es5-shim');
require('es5-sham');
require('jquery');

var angular = require('angular');

require('angular-route');
require('angular-ui-bootstrap');
require('ngstorage');

var app = angular.module('baseApp', ['ngRoute', 'ui.bootstrap','ngStorage']);

app.constant('VERSION', require('../../package.json').version);
app.constant('NAME', require('../../package.json').name);
app.constant('DESCRIPTION', require('../../package.json').description);

require('./factories');
require('./services');
require('./controllers');

app.config(function ($routeProvider,$locationProvider) {
  $locationProvider.html5Mode({
    enabled: true,
    requireBase: false
  });
  $routeProvider
    .when('/main', {
      templateUrl: 'views/main.html',
      controller: 'MainCtrl',
    })
    .when('/about', {
      templateUrl: 'views/about.html',
      controller: 'FooterCtrl',
    })
    .when('/accessToken', {
      templateUrl: 'views/auth.html',
      controller: 'AuthCtrl',
    })       
    .otherwise({
      redirectTo: '/main',
    });
});

app.run(function() {
  
});
