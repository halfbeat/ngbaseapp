'use strict';

module.exports = function() {
  var title='default';
  return {
    title: function() { return title; },
    setTitle: function(newTitle) { title = newTitle }
  };
};