'use strict';

module.exports = function($scope, Page) {
  $scope.Page = Page;
  $scope.Page.setTitle('Main Page');
  $scope.list = [];

  $scope.login=function() {
    var client_id="auth";
    var scope="email";
    var redirect_uri="http://localhost:8080/accessToken";
    var response_type="code";
    var url="https://10.16.177.104:8443/cas/oauth2.0/authorize?scope="+scope+"&client_id="+client_id+"&redirect_uri="+redirect_uri+"&response_type="+response_type;
    //var url="http://jgssc0007p843.jcyl.red:7001/authz/services/oauth2/authz?scope="+scope+"&response_type="+response_type+"&client_id="+client_id+"&redirect_uri="+redirect_uri+"&response_type="+response_type;
        
    window.location.replace(url.split('#')[0]);
  };
};
