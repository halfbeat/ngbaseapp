'use strict';

var app = require('angular').module('baseApp');

app.controller('HeaderCtrl', require('./header'));
app.controller('MainCtrl', require('./main'));
app.controller('AuthCtrl', require('./auth'));
app.controller('FooterCtrl', require('./footer'));
