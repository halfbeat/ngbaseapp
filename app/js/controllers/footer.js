'use strict';

module.exports = function($scope, NAME, DESCRIPTION, VERSION) {
  $scope.name = NAME;
  $scope.description = DESCRIPTION;
  $scope.version = VERSION;
};
