'use strict';

module.exports = function($scope, $http, $log, $location, $sessionStorage) {

    $scope.storage = $sessionStorage;
    var client_id='auth';
    var client_secret='auth';
    var code=$location.search().code;
    var redirect_uri='http://localhost:8080/accessToken';
    var url="https://10.16.177.104:8443/cas/oauth2.0/accessToken?client_id="+client_id+"&client_secret="+client_secret+"&redirect_uri="+redirect_uri+"&code="+code;        

    function getParameterByName(name, url) {
        if (!url) {
            url = window.location.href;
        }
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp(name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }   
    $http.get(url).then(
        function(response) {     
            $sessionStorage.authorization_data = {
              access_token: getParameterByName('access_token', response.data),
              expires_in: getParameterByName('expires_in', response.data),
            };   
        },
        function(response) {            
            $log.log('ERROR ' + response);
        }
    );    
};
