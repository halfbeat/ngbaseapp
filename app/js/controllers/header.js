'use strict';

module.exports = function ($scope, NAME, DESCRIPTION) {
  $scope.aplicacion = {
    nombre: NAME,
    descripcion: DESCRIPTION
  };
};
