'use strict';

var browserify = require('browserify')
  , del = require('del')
  , source = require('vinyl-source-stream')
  , vinylPaths = require('vinyl-paths')
  , glob = require('glob')
  , Server = require('karma').Server
  , cleanCSS = require('gulp-clean-css')
  , protractor = require("gulp-protractor").protractor
  , gulp = require('gulp')
  , history = require('connect-history-api-fallback');

// Load all gulp plugins listed in package.json
var gulpPlugins = require('gulp-load-plugins')({
  pattern: ['gulp-*', 'gulp.*'],
  replaceString: /\bgulp[\-.]/
});

// Define file path variables
var paths = {
  root: 'app/',      // App root path
  src:  'app/js/',   // Source path
  dist: 'app/dist/', // Distribution path
  test: 'test/',     // Test path
};

/*
 * Useful tasks:
 * - gulp fast:
 *   - linting
 *   - unit tests
 *   - browserification
 *   - no minification, does not start server.
 * - gulp watch:
 *   - starts server with live reload enabled
 *   - lints, unit tests, browserifies and live-reloads changes in browser
 *   - no minification
 * - gulp:
 *   - linting
 *   - unit tests
 *   - browserification
 *   - minification and browserification of minified sources
 *   - start server for e2e tests
 *   - run Protractor End-to-end tests
 *   - stop server immediately when e2e tests have finished
 *
 * At development time, you should usually just have 'gulp watch' running in the
 * background all the time. Use 'gulp' before releases.
 */

var liveReload = true;

gulp.task('clean', function () {
  return gulp
    .src([paths.root + 'ngAnnotate', paths.dist], { read: false })
    .pipe(vinylPaths(del));
});

gulp.task('lint', function () {
  return gulp
    .src(['gulpfile.js',
      paths.src + '**/*.js',
      paths.test + '**/*.js',
      '!' + paths.src + 'third-party/**',
      '!' + paths.test + 'browserified/**',
    ])
    .pipe(gulpPlugins.eslint())
    .pipe(gulpPlugins.eslint.format());
});

gulp.task('copy-fonts', function () {
  return gulp.src('node_modules/bootstrap/dist/fonts/**/*.{ttf,woff,eof,svg,woff2,eot}')
    .pipe(gulp.dest(paths.dist + '/fonts'));
});

gulp.task('minify-css', ['copy-fonts'], function () {
  return gulp.src(paths.root + 'css/app.css')
    .pipe(cleanCSS({ debug: true, processImport: true }, function (details) {
      console.log(details.name + ': ' + details.stats.originalSize);
      console.log(details.name + ': ' + details.stats.minifiedSize);
    }))
    .pipe(gulp.dest(paths.dist + '/css'));
});

gulp.task('unit', function () {
  return gulp.src([
    paths.test + 'unit/**/*.js'
  ])
    .pipe(gulpPlugins.mocha({ reporter: 'dot' }));
});

gulp.task('browserify', /*['lint', 'unit'],*/ function () {
  return browserify(paths.src + 'app.js', { debug: true })
    .bundle()
    .pipe(source('app.js'))
    .pipe(gulp.dest(paths.dist))
    .pipe(gulpPlugins.connect.reload());
});

gulp.task('ngAnnotate', ['lint', 'unit'], function () {
  return gulp.src([
    paths.src + '**/*.js',
    '!' + paths.src + 'third-party/**',
  ])
    .pipe(gulpPlugins.ngAnnotate())
    .pipe(gulp.dest(paths.root + 'ngAnnotate'));
});

gulp.task('browserify-min', ['ngAnnotate'], function () {
  return browserify(paths.root + 'ngAnnotate/app.js')
    .bundle()
    .pipe(source('app.min.js'))
    .pipe(gulpPlugins.streamify(gulpPlugins.uglify({ mangle: false })))
    .pipe(gulp.dest(paths.dist));
});

gulp.task('browserify-tests', function () {
  var bundler = browserify({ debug: true });
  glob
    .sync(paths.test + 'unit/**/*.js')
    .forEach(function (file) {
      bundler.add(file);
    });
  return bundler
    .bundle()
    .pipe(source('browserified_tests.js'))
    .pipe(gulp.dest(paths.test + 'browserified'));
});

gulp.task('karma', ['browserify-tests'], function (done) {
  new Server({
    configFile: __dirname + '/karma.conf.js',
    singleRun: true
  }, done).start();
});

var cors = function (req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
  res.setHeader('Access-Control-Allow-Headers', 'Cache-Control, Pragma, Origin, Authorization, Content-Type, X-Requested-With');
  next();
};

gulp.task('server', ['minify-css', 'browserify'], function () {  
  gulpPlugins.connect.server({
    root: 'app',
    livereload: liveReload,
    middleware: function(connect, opt) {
      return [
        cors,
        history({})        
      ]
    }    
  });
});

gulp.task('e2e', ['minify-css', 'browserify'], function () {
  gulpPlugins.connect.server({
    root: 'app'
  });
  return gulp.src([paths.test + 'e2e/**/*.js'])
    .pipe(protractor({
      configFile: 'protractor.conf.js',
      args: ['--baseUrl', 'http://127.0.0.1:8080'],
    }))
    .on('error', function (e) {
      gulpPlugins.connect.serverClose();
      throw e;
    })
    .on('end', function () {
      gulpPlugins.connect.serverClose();
    });
});

gulp.task('watch', function () {
  gulp.start('server');
  gulp.watch([
    paths.src + '**/*.js',
    '!' + paths.src + 'third-party/**',
    paths.test + '**/*.js',
    paths.root + '**/*.css',
  ], ['fast']);
});

gulp.task('fast', [], function () {
  gulp.start('browserify');
});

gulp.task('default', ['clean'], function () {
  liveReload = false;
  gulp.start('karma', 'browserify', 'browserify-min', 'e2e');
});
