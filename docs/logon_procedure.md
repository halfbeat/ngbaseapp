Caso de uso principal
1.   Se recupera la aplicación cliente desde el servidor y se carga en el navegador
2.   La aplicación cliente inicia su arranque.
3.   La aplicación cliente busca en el localStorage las propiedades accessToken y accessTokenExpiration.
4.   No existen ambas propiedades o existe la primera y la segund ha expirado.
5.   Se redirige al agente de usuario (browser) a la url de autorización OAuthz2 de SSO. Se indican la url de retorno y el identificador del cliente.
6.   El SSO comprueba que usuario no está logado o que ha expirado su sesión. 
7.   Se piden las credenciales al usuario.
8.   El usuario introduce sus credenciales.
9.   El sistema valida con éxito las credenciales y redirige al navegador del usuario a la url de retorno con un código de un solo uso.
10.  La aplicación cliente recupera el código de retorno y realiza una petición a la url de obtención de token de autorización del SSO. 
     Se pasan como parámetros la url de retorno, el identificador y clave del cliente y el código recuperado.
11.  El SSO comprueba los parámetros y genera un token de acceso que devuelve en el cuerpo de la respuesta.
12.  La aplicación cliente almacena en el 

4.a.1      Se redirige al agente de usuario (browser) a la url de autorización de la aplicación en el OAuthz2 de SSO. 
4.a.2.a    El usuario no está logado en el SSO o ha expirado su sesión:
4.a.2.a.1  Se piden las credenciales al usuario
4.a.2.a.2  El usuario introduce sus credenciales. El sistema las valida. Si no son correctas se vuelve al paso 4.a.2.a.1

Si no existen 
Si no estamos logados se nos pedirán las credenciales en CAS que introduciremos y enviaremos o se nos redirigirá a la aplicación cliente con un código de validación.
La aplicación cliente utilizará este código junto con su identificador y clave para obtener un token de acceso.
En caso de éxito se almacenará en el localStorage las propiedades accessToken y accessTokenExpiration.

