exports.config = {
  seleniumServerJar: 'node_modules/webdriver-manager/selenium/selenium-server-standalone-3.0.1.jar',

  capabilities: {
    'browserName': 'chrome'
  },  
  specs: ['test/e2e/**/*_spec.js'],

  rootElement: 'body',

  jasmineNodeOpts: {
    showColors: true,
    defaultTimeoutInterval: 30000,
    isVerbose: false,
    includeStackTrace: false
  }
};
